# python-imageio-ffmpeg

FFMPEG wrapper for Python

https://github.com/imageio/imageio-ffmpeg

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/tartube/python-imageio-ffmpeg.git
```

